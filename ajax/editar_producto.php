<?php
	if (empty($_POST['edit_id'])){
		$errors[] = "ID está vacío.";
	} elseif (!empty($_POST['edit_id'])){
	require_once ("../php/conexion.php");

	$nombre = mysqli_real_escape_string($con,(strip_tags($_POST["edit_nombre"],ENT_QUOTES)));
	$descripcion = mysqli_real_escape_string($con,(strip_tags($_POST["edit_descripcion"],ENT_QUOTES)));
	$costo = floatval($_POST["edit_costo"]);

	$file = $_FILES["edit_imagen"];
	$nombre_file = $file["name"];
	$tipo = $file["type"];
	$ruta_provicional = $file["tmp_name"];
	$size = $file["size"];
	$carpeta = "uploads/";

	$imagen = $carpeta.$nombre_file;



	$id=intval($_POST['edit_id']);
	// UPDATE data into database
    $sql = "UPDATE producto SET nombre='".$nombre."', descripcion='".$descripcion."', costo='".$costo."',  imagen='".$imagen."' WHERE id='".$id."' ";
    $query = mysqli_query($con,$sql);

    move_uploaded_file($ruta_provicional,$imagen);

    // if product has been added successfully
    if ($query) {
        $messages[] = "El producto ha sido actualizado con éxito.";
    } else {
        $errors[] = "Lo sentimos, la actualización falló. Por favor, regrese y vuelva a intentarlo.";
    }
		
	} else 
	{
		$errors[] = "desconocido.";
	}
if (isset($errors)){
			
			?>
			<div class="alert alert-danger" role="alert">
				<button type="button" class="close" data-dismiss="alert">&times;</button>
					<strong>Error!</strong> 
					<?php
						foreach ($errors as $error) {
								echo $error;
							}
						?>
			</div>
			<?php
			}
			if (isset($messages)){
				
				?>
				<div class="alert alert-success" role="alert">
						<button type="button" class="close" data-dismiss="alert">&times;</button>
						<strong>¡Bien hecho!</strong>
						<?php
							foreach ($messages as $message) {
									echo $message;
								}
							?>
				</div>
				<?php
			}
?>			