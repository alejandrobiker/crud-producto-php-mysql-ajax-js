<?php
	if (empty($_POST['nombre'])){
		$errors[] = "Ingresa el nombre del producto.";
	} elseif (!empty($_POST['nombre']) &&
				isset($_FILES['imagen']) && !empty($_FILES['imagen'])){
	require_once ("../php/conexion.php");
	
	$nombre = mysqli_real_escape_string($con,(strip_tags($_POST["nombre"],ENT_QUOTES)));
	$descripcion = mysqli_real_escape_string($con,(strip_tags($_POST["descripcion"],ENT_QUOTES)));
	$costo = floatval($_POST["costo"]);

	$file = $_FILES["imagen"];
	$nombre_file = $file["name"];
	$tipo = $file["type"];
	$ruta_provicional = $file["tmp_name"];
	$size = $file["size"];
	$carpeta = "uploads/";

	$imagen = $carpeta.$nombre_file;

	// REGISTER data into database
    $sql = "INSERT INTO producto(id, nombre, descripcion, costo, imagen, fecha) VALUES (NULL,'$nombre','$descripcion','$costo','$imagen',NOW())";
    $query = mysqli_query($con,$sql);
    
    move_uploaded_file($ruta_provicional,$imagen);

    // if product has been added successfully
    if ($query) {
        $messages[] = "El producto ha sido guardado con éxito.";
    } else {
        $errors[] = "Lo sentimos, el registro falló. Por favor, regrese y vuelva a intentarlo.";
    }
		
	} else 
	{
		$errors[] = "desconocido.";
	}
	
	if (isset($errors)){
			
		?>
		<div class="alert alert-danger" role="alert">
			<button type="button" class="close" data-dismiss="alert">&times;</button>
				<strong>Error!</strong> 
				<?php
					foreach ($errors as $error) {
							echo $error;
						}
					?>
		</div>
		<?php
		}
		if (isset($messages)){
			
			?>
			<div class="alert alert-success" role="alert">
					<button type="button" class="close" data-dismiss="alert">&times;</button>
					<strong>¡Bien hecho!</strong>
					<?php
						foreach ($messages as $message) {
								echo $message;
							}
						?>
			</div>
			<?php
		}
?>	
