<?php
	
	/* Connect To Database*/
	require_once ("../php/conexion.php");

	$dolar = 74;

$action = (isset($_REQUEST['action'])&& $_REQUEST['action'] !=NULL)?$_REQUEST['action']:'';
if($action == 'ajax'){
	$query = mysqli_real_escape_string($con,(strip_tags($_REQUEST['query'], ENT_QUOTES)));

	$tables="producto";
	$campos="*";
	$sWhere=" producto.nombre LIKE '%".$query."%'";
	$sWhere.=" order by producto.id DESC";
	
	
	include 'pagination.php'; //include pagination file
	//pagination variables
	$page = (isset($_REQUEST['page']) && !empty($_REQUEST['page']))?$_REQUEST['page']:1;
	$per_page = intval($_REQUEST['per_page']); //how much records you want to show
	$adjacents  = 4; //gap between pages after number of adjacents
	$offset = ($page - 1) * $per_page;
	//Count the total number of row in your table*/
	$count_query   = mysqli_query($con,"SELECT count(*) AS numrows FROM $tables where $sWhere ");
	if ($row= mysqli_fetch_array($count_query)){$numrows = $row['numrows'];}
	else {echo mysqli_error($con);}
	$total_pages = ceil($numrows/$per_page);
	//main query to fetch the data
	$query = mysqli_query($con,"SELECT $campos FROM  $tables where $sWhere LIMIT $offset,$per_page");
	//loop through fetched data
	

	if ($numrows>0){
		
	?>
		<div class="table-responsive">
			<table class="table table-striped table-hover">
				<thead>
					<tr>
						<th class='text-left'>Nombre</th>
						<th>Descripción </th>
						<th class='text-right'>Precio $</th>
						<!-- <th class='text-right'>Precio bs</th> -->
						<th class='text-right'>Imagen</th>
						<th class='text-right'>Fecha</th>						
						<th>Acciones</th>
					</tr>
				</thead>
				<tbody>	
						<?php 
						$finales=0;
						while($row = mysqli_fetch_array($query)){	
							$id=$row['id'];
							$nombre=$row['nombre'];
							$descripcion=$row['descripcion'];
							$costo=$row['costo'];	
							// $dolarToday = $row['costo'] / $dolar;							
							$imagen=$row['imagen'];						
							$fecha=$row['fecha'];						
							$finales++;
						?>	
						<tr class="<?php echo $text_class;?>">
							<td class='text-left'><?php echo $nombre;?></td>
							<td ><?php echo $descripcion;?></td>
							<td class='text-right'><?php echo number_format($costo,2);?></td>
							<!-- <td class='text-right'><?php echo number_format($dolarToday,2);?></td> -->
							<td class='text-right'><img src='ajax/<?php echo $imagen?>' style="width: 50px; height: 50px;"></td>
							<td class='text-right'><?php echo $fecha; ?></td>
							<td>
								<a href="#"  data-target="#editProductModal" class="edit" data-toggle="modal" data-nombre='<?php echo $nombre;?>' data-descripcion="<?php echo $descripcion?>" data-costo="<?php echo $costo?>" data-fecha="<?php echo $fecha;?>" data-id="<?php echo $id; ?>"><i class="material-icons" data-toggle="tooltip" title="Editar" >&#xE254;</i></a>
								
								<a href="#deleteProductModal" class="delete" data-toggle="modal" data-id="<?php echo $id;?>"><i class="material-icons" data-toggle="tooltip" title="Eliminar">&#xE872;</i></a>
                    		</td>
						</tr>
						<?php }?>
						<tr>
							<td colspan='7'> 
								<?php 
									$inicios=$offset+1;
									$finales+=$inicios -1;
									echo "Mostrando $inicios al $finales de $numrows registros";
									echo paginate( $page, $total_pages, $adjacents);
								?>
							</td>
						</tr>
				</tbody>			
			</table>
		</div>	

	
	
	<?php	
	}	
}
?>          
		  
