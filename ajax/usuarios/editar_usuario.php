<?php
session_start();
include("../../php/conexion.php");

	if (isset($_POST['edit_idUsuario']) && !empty ($_POST['edit_idUsuario'])){
		$errors[] = "ID está vacío.";
	} elseif (!empty($_POST['edit_idUsuario'])){
	
	$nombre = mysqli_real_escape_string($con,(strip_tags($_POST["edit_nombreUsuario"],ENT_QUOTES)));
	$apellido = mysqli_real_escape_string($con,(strip_tags($_POST["edit_apellido"],ENT_QUOTES)));
	$email = mysqli_real_escape_string($con,(strip_tags($_POST["edit_email"],ENT_QUOTES)));
	$rol = mysqli_real_escape_string($con,(strip_tags($_POST["edit_rol"],ENT_QUOTES)));

	$id=intval($_POST['edit_idUsuario']);
	// UPDATE data into database
    $sql = "UPDATE usuario SET nombre='".$nombre."', apellido='".$apellido."', email='".$email."',  rol='".$rol."' WHERE id='".$id."' ";
    $query = mysqli_query($con,$sql);

    // if product has been added successfully
    if ($query) {
        $messages[] = "El usuario ha sido actualizado con éxito.";
    } else {
        $errors[] = "Lo sentimos, la actualización falló. Por favor, regrese y vuelva a intentarlo.";
    }
		
	}

	if (isset($errors)){
			
		?>
		<div class="alert alert-danger" role="alert">
			<button type="button" class="close" data-dismiss="alert">&times;</button>
				<strong>Error!</strong> 
				<?php
					foreach ($errors as $error) {
							echo $error;
						}
					?>
		</div>
		<?php
	}
	if (isset($messages)){
		
		?>
		<div class="alert alert-success" role="alert">
				<button type="button" class="close" data-dismiss="alert">&times;</button>
				<strong>¡Bien hecho!</strong>
				<?php
					foreach ($messages as $message) {
							echo $message;
						}
				?>
		</div>
	<?php 
	}elseif(isset($_POST['nclave']) && !empty($_POST['nclave']) &&
	isset($_POST['rclave']) && !empty($_POST['rclave']) &&
	($_POST['nclave'] == $_POST['rclave'])) {

		// $consulta = "SELECT * FROM usuario WHERE id = '$_SESSION[userid]' AND clave = '$_POST[clave_vieja]";

		$sql = "UPDATE usuario SET clave='$_POST[nclave]' WHERE id = '$_SESSION[userid]'";							
		$query = mysqli_query($con,$sql);	
		
		?>
			<div class="alert alert-success" role="alert">
				<button type="button" class="close" data-dismiss="alert">&times;</button>
				<strong>¡Bien hecho!</strong> clave modificada. 
			</div>
		<?php			
			
			
	}elseif($_POST['nclave'] != $_POST['rclave']){
		?>
		<div class="alert alert-danger" role="alert">
			<button type="button" class="close" data-dismiss="alert">&times;</button>
				<strong>Error!</strong> Las claves no coinciden. 				
		</div>
	<?php 
	}



?>			