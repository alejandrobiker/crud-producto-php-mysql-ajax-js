<?php
session_start();
	if (empty($_POST['delete_idUsuario'])){
		$errors[] = "Id vacío.";
	} elseif (!empty($_POST['delete_idUsuario'])){
		require_once ("../../php/conexion.php");//Contiene funcion que conecta a la base de datos
		// escaping, additionally removing everything that could be (html/javascript-) code
	    $id=intval($_POST['delete_idUsuario']);
	    $email = $_POST["delete_emailUsuario"];

	    if($_SESSION['useremail'] != $email){
			// DELETE FROM  database
		    $sql = "DELETE FROM usuario WHERE id='$id'";
		    $query = mysqli_query($con,$sql);	    
	 
	        $messages[] = "El usuario ha sido eliminado con éxito.";

	    } else {
	        $errors[] = "Lo sentimos, la eliminación falló. Recuerde que no se puede eliminar su propia cuenta.";
	    }
			
	} else	{
		$errors[] = "desconocido.";
	}
if (isset($errors)){
			
			?>
			<div class="alert alert-danger" role="alert">
				<button type="button" class="close" data-dismiss="alert">&times;</button>
					<strong>Error!</strong> 
					<?php
						foreach ($errors as $error) {
								echo $error;
							}
						?>
			</div>
			<?php
			}
			if (isset($messages)){
				
				?>
				<div class="alert alert-success" role="alert">
						<button type="button" class="close" data-dismiss="alert">&times;</button>
						<strong>¡Bien hecho!</strong>
						<?php
							foreach ($messages as $message) {
									echo $message;
								}
							?>
				</div>
				<?php
			}
?>			