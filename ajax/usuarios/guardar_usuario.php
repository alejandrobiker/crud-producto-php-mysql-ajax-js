<?php
	if (empty($_POST['nombres'])){
		$errors[] = "Ingresa el nombre del usuario.";
	} elseif (!empty($_POST['nombres'])){
	
		require_once ("../../php/conexion.php");
	
		$nombre = mysqli_real_escape_string($con,(strip_tags($_POST["nombres"],ENT_QUOTES)));
		$apellido = mysqli_real_escape_string($con,(strip_tags($_POST["apellidos"],ENT_QUOTES)));
		$email = mysqli_real_escape_string($con,(strip_tags($_POST["email"],ENT_QUOTES)));
		$clave = mysqli_real_escape_string($con,(strip_tags($_POST["clave"],ENT_QUOTES)));
		$rclave = mysqli_real_escape_string($con,(strip_tags($_POST["rclave"],ENT_QUOTES)));
		$rol = mysqli_real_escape_string($con,(strip_tags($_POST["rol"],ENT_QUOTES)));
		
		if ($clave != $rclave) {

			$errors[] = "Las claves no coinciden.";

		}elseif($clave == $rclave){
			// REGISTER data into database
	   	 	$sql = "INSERT INTO usuario(id, nombre, apellido, email, clave, rol, fecha) VALUES (NULL,'$nombre','$apellido','$email','$clave', '$rol' ,NOW())";
	   	 	$query = mysqli_query($con,$sql);

	   	 	$messages[] = "El usuario ha sido guardado con éxito.";

	   	} else {

	        $errors[] = "Lo sentimos, el registro falló. Por favor, regrese y vuelva a intentarlo.";

	    }
		
	} else	{
		$errors[] = "Desconocido.";
	}
	
	if (isset($errors)){
			
		?>
		<div class="alert alert-danger" role="alert">
			<button type="button" class="close" data-dismiss="alert">&times;</button>
				<strong>Error!</strong> 
				<?php
					foreach ($errors as $error) {
							echo $error;
						}
					?>
		</div>
		<?php
		}
		if (isset($messages)){
			
			?>
			<div class="alert alert-success" role="alert">
					<button type="button" class="close" data-dismiss="alert">&times;</button>
					<strong>¡Bien hecho!</strong>
					<?php
						foreach ($messages as $message) {
								echo $message;
							}
						?>
			</div>
			<?php
		}
?>	
