<h2 class="font-weight-bold titulo mt-5">Contacto</h2>
    
    <form id="formularioContacto" class="mbr-form" method="post">
        <div class="row row-sm-offset">
            <div class="col-md-4" data-for="name">
                <div class="form-group">
                    <label class="form-control-label mbr-fonts-style display-7" for="name-form1-2w">Nombre</label>
                    <input type="text" class="form-control" name="name" data-form-field="Name" required id="name">
                </div>
            </div>
            <div class="col-md-4" data-for="email">
                <div class="form-group">
                    <label class="form-control-label mbr-fonts-style display-7" for="email">Correo</label>
                    <input type="email" class="form-control" name="email" data-form-field="Email" required id="email">
                </div>
            </div>
            <div class="col-md-4" data-for="phone">
                <div class="form-group">
                    <label class="form-control-label mbr-fonts-style display-7" for="phone">Teléfono</label>
                    <input type="tel" class="form-control" name="phone" data-form-field="Phone" id="phone" required>
                </div>
            </div>
        </div>
        <div class="form-group" data-for="message">
            <label class="form-control-label mbr-fonts-style display-7" for="message">Mensaje</label>
            <textarea type="text" class="form-control" name="message" rows="7" data-form-field="Message" id="message" required></textarea>
        </div>

        <div class="col clearfix">
            <span class="input-group-btn right">
                <button href="" type="submit" class="btn btn-rosado" style="margin-right: 5px;">Enviar correo</button>                
                <button type="reset" class="btn btn-default">Limpiar campos</button>
            </span>
            <hr>
            <div id="resultado"></div>
        </div>
    </form>

    <script src="js/script.js"></script>
