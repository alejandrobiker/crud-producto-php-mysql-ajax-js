<div class="col-md-12">	  

    <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
      <!-- Indicators -->
      	<ol class="carousel-indicators">
	        <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
	        <li data-target="#carousel-example-generic" data-slide-to="1"></li>
	        <li data-target="#carousel-example-generic" data-slide-to="2"></li>
          <li data-target="#carousel-example-generic" data-slide-to="3"></li>
          <li data-target="#carousel-example-generic" data-slide-to="4"></li>
      	</ol>

      	<!-- Wrapper for slides -->
      	<div class="carousel-inner">
	        <div class="item active">
          		<img class="carru" src="img/carrousel/carrousel_5.jpg" alt="...">
	          	<div class="carousel-caption">
            	  <h2>Alfanjores</h2>
          	  </div>
    	    </div>
          <div class="item">
          	<img src="img/carrousel/carrousel_2.jpg" alt="...">
          	<div class="carousel-caption">
            	<h2>Ponque</h2>
          	</div>
          </div>
    	    <div class="item">
          	<img src="img/carrousel/carrousel_3.jpg" alt="...">
          	<div class="carousel-caption">
            	<h2>Dulces de colores</h2>
          	</div>
          </div>
          <div class="item">
            <img src="img/carrousel/carrousel_4.png" alt="...">
            <div class="carousel-caption">
              <h2>Dulce de corazones</h2>
            </div>
          </div>
          <div class="item">
            <img src="img/carrousel/carrousel_1.jpg" alt="...">
            <div class="carousel-caption">
              <h2>Torta de fresa</h2>
            </div>
          </div>
  		  </div>

      	<!-- Controls -->
      	<a class="left carousel-control" href="#carousel-example-generic" data-slide="prev">
	        <span class="glyphicon glyphicon-chevron-left"></span>
      	</a>
      	<a class="right carousel-control" href="#carousel-example-generic" data-slide="next">
	        <span class="glyphicon glyphicon-chevron-right"></span>
      	</a>
    </div><br><br>				  
</div>