<footer class="pink lighten-3 mt-4">	  
	  	<div class="container text-center text-md-left">	   
	   		<div class="row">		     
			    <div class="col-md-4 mx-auto text-center mb-4 mt-2">
			        <h5 class="font-weight-bold letra_grande">Redes sociales</h5>
			        <ul class="list-unstyled">
				        <li>
				            <a href="#!" class="icon_redes"><i class="fab fa-facebook fa-2x"></i></a>
				            <!-- <a href="#!"><i class="fab fa-twitter fa-2x"></i></a> -->
				            <a href="https://www.instagram.com/dulcesfamily/" target="blank_" class="icon_redes"><i class="fab fa-instagram fa-2x"></i></a>
				            <a href="#!" class="icon_redes"><i class="fab fa-youtube fa-2x"></i></a>
				        </li>
			        </ul>
			    </div>	    
		     
		     	<div class="col-md-4 mx-auto text-center mb-4 mt-2">
			        <h5 class="font-weight-bold letra_grande">Ubicación</h5>
			        <ul class="list-unstyled">
			          	<li>
			            	<p><b>Dirección: </b>Catia - 23 de enero</p>
			          	</li>
			        </ul>
		     	</div>

		     	<div class="col-md-4 mx-auto text-center mb-4 mt-2">
			        <h5 class="font-weight-bold letra_grande">Escríbenos</h5>
			        <ul class="list-unstyled">
			          	<li>			            
			            	<p><b>Email: </b>dulcefamily@gmail.com</p>
			            	<p><b>Teléfono: </b>+58 414 - 151.48.53</p>
			          	</li>
			        </ul>
		     	</div>	 
	    	</div>
		</div>
		<div class="text-center pink lighten-3 mb-1">Copyright © 2020, Dulces Family</div>
	</footer>