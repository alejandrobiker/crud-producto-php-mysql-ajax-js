<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Dulces Family </title>
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.11.2/css/all.css">
	<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto|Varela+Round">
	<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
	<link rel="stylesheet" href="css/bootstrap.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
	<link rel="stylesheet" href="css/custom.css">

	<script type="text/javascript">
		$(document).ready(function() {

		    $('#boton2').on('click', function() {
		        $('.navbar-nav li').removeClass('active');
		        $("#info").load('productos.php');
		        return false;
		    });

		    $('#boton3').on('click', function() {
		        $('.navbar-nav li').removeClass('active');
		        $("#info").load('contacto.php');
		        return false;
		    });

		    $('#boton4').on('click', function() {
		        $('.navbar-nav li').removeClass('active');
		        $("#info").load('productos_table.php');
		        return false;
		    });
		    $('#boton5').on('click', function() {
		        $('.navbar-nav li').removeClass('active');
		        $("#info").load('usuarios_table.php');
		        return false;
		    });
		});
	</script>