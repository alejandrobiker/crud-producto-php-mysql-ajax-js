<div id="addProductModal" class="modal fade">
	<div class="modal-dialog">
		<div class="modal-content">
			<form name="add_product" id="add_product" enctype="multipart/form-data">
				<div class="modal-header">						
					<h4 class="modal-title">Agregar Producto</h4>
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				</div>
				<div class="modal-body">					
					<div class="form-group">
						<label>Producto</label>
						<input type="text" name="nombre" id="nombre" class="form-control" required>
					</div>
					<div class="form-group">
						<label>Descripción</label>
						<textarea name="descripcion" id="descripcion" class="form-control" required></textarea>
					</div>
					<div class="form-group">
						<label>Precio en $</label>
						<input type="number" name="costo" id="costo" class="form-control" required>
					</div>	
					<div class="form-group">
						<label>Imagen</label>
						<input type="file" name="imagen" id="imagen" class="form-control" accept="image" required multiple>
					</div>		

				</div>
				<div class="modal-footer">
					<input type="button" class="btn btn-default" data-dismiss="modal" value="Cancelar">
					<input type="submit" class="btn btn-success" value="Guardar datos">
				</div>
			</form>
		</div>
	</div>
</div>

<div id="addUsuarioModal" class="modal fade">
	<div class="modal-dialog">
		<div class="modal-content">
			<form name="add_usuario" id="add_usuario">
				<div class="modal-header">						
					<h4 class="modal-title">Agregar Usuario</h4>
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				</div>
				<div class="modal-body">					
					<div class="form-group">
						<label>Nombres</label>
						<input type="text" name="nombres" id="nombres" class="form-control" required>
					</div>
					<div class="form-group">
						<label>Apellidos</label>
						<input type="text" name="apellidos" id="apellidos" class="form-control" required>
					</div>
					<div class="form-group">
						<label>Email</label>
						<input type="email" name="email" id="email" class="form-control" required>
					</div>
					<div class="form-group">
						<label>Clave</label>
						<input type="password" name="clave" id="clave" class="form-control" minlength="5" required>
					</div>
					<div class="form-group">
						<label>Repetir clave</label>
						<input type="password" name="rclave" id="rclave" class="form-control" minlength="5" required>
					</div>						
					<div>
						<label for="rol">Rol:</label>
						<select id="rol" name="rol" class="form-control">
						  <option value="Administrador">Administrador</option>
						  <option value="Usuario">Usuario</option>
						</select>
					</div>

				</div>
				<div class="modal-footer">
					<input type="button" class="btn btn-default" data-dismiss="modal" value="Cancelar">
					<input type="submit" class="btn btn-success" value="Guardar datos">
				</div>
			</form>
		</div>
	</div>
</div>