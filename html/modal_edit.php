<!-- Editar producto -->
<div id="editProductModal" class="modal fade">
	<div class="modal-dialog">
		<div class="modal-content">
			<form name="edit_product" id="edit_product">
				<div class="modal-header">						
					<h4 class="modal-title">Editar Producto</h4>
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				</div>
				<div class="modal-body">				
					<div class="form-group">
						<label>Producto</label>
						<input type="hidden" name="edit_id" id="edit_id" >
						<input type="text" name="edit_nombre" id="edit_nombre" class="form-control" required>
					</div>
					<div class="form-group">
						<label>Descripción</label>
						<textarea name="edit_descripcion" id="edit_descripcion" class="form-control" required></textarea>
					</div>
					<div class="form-group">
						<label>Precio en $</label>
						<input type="number" name="edit_costo" id="edit_costo" class="form-control" required>
					</div>
					<div class="form-group">
						<label>Imagen</label>											
						<input type="file" name="edit_imagen" id="edit_imagen" class="form-control" required>
					</div>					
				</div>
				<div class="modal-footer">
					<input type="button" class="btn btn-default" data-dismiss="modal" value="Cancelar">
					<input type="submit" class="btn btn-info" value="Guardar Cambios">
				</div>
			</form>
		</div>
	</div>
</div>

<!-- Editar usuario -->
<div id="editUsuarioModal" class="modal fade">
	<div class="modal-dialog">
		<div class="modal-content">
			<form name="edit_usuario" id="edit_usuario">
				<div class="modal-header">						
					<h4 class="modal-title">Editar Usuario</h4>
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				</div>
				<div class="modal-body">				
					<div class="form-group">
						<label>Nombres</label>
						<input type="hidden" name="edit_idUsuario" id="edit_idUsuario" >
						<input type="text" name="edit_nombreUsuario" id="edit_nombreUsuario" class="form-control" required>
					</div>
					<div class="form-group">
						<label>Apellidos</label>
						<input type="text" name="edit_apellido" id="edit_apellido" class="form-control" required>
					</div>
					<div class="form-group">
						<label>Email</label>
						<input type="email" name="edit_email" id="edit_email" class="form-control" required>
					</div>		
					<div>
						<label for="rol">Rol:</label>
						<select id="edit_rol" name="edit_rol" class="form-control">
						  <option value="Administrador">Administrador</option>
						  <option value="Usuario">Usuario</option>
						</select>
					</div>		
				</div>
				<div class="modal-footer">
					<input type="button" class="btn btn-default" data-dismiss="modal" value="Cancelar">
					<input type="submit" class="btn btn-info" value="Guardar datos">
				</div>
			</form>
		</div>
	</div>
</div>

<!-- Cambiar clave -->
<div id="ClaveUsuario" class="modal fade">
		<div class="modal-dialog">
			<div class="modal-content">
				<form name="cambioClave" id="cambioClave" method="post">
					<div class="modal-header">						
						<h4 class="modal-title">Cambiar clave</h4>
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					</div>
					<div class="modal-body">					
						<div class="form-group">
							<label>Nueva clave</label>
							<input type="password" name="nclave" id="nclave" class="form-control" minlength="5" required>
						</div>
						<div class="form-group">
							<label>Repetir clave</label>
							<input type="password" name="rclave" id="rclave" class="form-control" minlength="5" required>
						</div>						
					</div>
					<div id="resultado"></div>
					<div class="modal-footer">
						<button class="btn btn-primary" type="submit">Ingresar</button>
			    		<a class="btn btn-default" href="index">Atras</a>	
					</div>
				</form>
			</div>
		</div>
	</div>