	<div id="loginModal" class="modal fade">
		<div class="modal-dialog">
			<div class="modal-content">
				<form name="formularioSesion" id="formularioSesion" method="post">
					<div class="modal-header">						
						<h4 class="modal-title">Iniciar sesión</h4>
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					</div>
					<div class="modal-body">					
						<div class="form-group">
							<label>Email</label>
							<input type="text" name="email" id="email" class="form-control" required>
						</div>
						<div class="form-group">
							<label>Contraseña</label>
							<input type="password" name="clave" id="clave" class="form-control" required>
						</div>						
					</div>
					<div class="modal-footer">
						<button class="btn btn-primary" type="submit">Ingresar</button>
			    		<a class="btn btn-default" href="index">Atras</a>	
					</div>
					<div id="ver"></div>
				</form>
			</div>
		</div>
	</div>
