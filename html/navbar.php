<nav class="navbar navbar-default">
	  	<div class="container-fluid">
		    <!-- Brand and toggle get grouped for better mobile display -->
		    <div class="navbar-header">
		      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
		        <span class="sr-only">Toggle navigation</span>
		        <span class="icon-bar"></span>
		        <span class="icon-bar"></span>
		        <span class="icon-bar"></span>
		      </button>
		      <img class="logito" src="img/logo/logo.png" height="40">
		    </div>

	    	<!-- Collect the nav links, forms, and other content for toggling -->
		    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
		      	<ul class="nav navbar-nav">
			        <li class="active"><a href="index">Inicio <span class="sr-only">(current)</span></a></li>
			        <li><a href="productos" id="boton2">Productos</a></li>
			        <li><a href="contacto" id="boton3">Contacto</a></li>       		        
			        <!--li><a href="productos_table" id="boton4">Admin productos</a></li-->
		          	<?php
						//session_start();
						ini_set('display_errors', 0);
									                
	                	if (session_start() == true){
	                		if($_SESSION['useremail'] != null && $_SESSION['userrol'] == "Administrador") {						      
			                	echo'
									<li>
										<a href="productos_table" id="boton4">Admin productos</a>
									</li>
									<li>
										<a href="usuarios_table" id="boton5">Admin Usuarios</a>
									</li>
								';
							} 
						}
					?> 				            
			          	        	
		     	</ul>

		      	<ul class="nav navbar-nav navbar-right">
		        	<li class="dropdown">
		          
				 	<?php
				 	// ini_set('display_errors', 0);
	   				 		if ($_SESSION['useremail'] == null) {
	   				 			echo'
	   				 				<a href="#loginModal" data-toggle="modal" class="dropdown-toggle" role="button">Iniciar sesión</a>	
	   				 			';
		 					}else{
			 					echo'
			 					<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">'.$_SESSION['usernombre'].' '.$_SESSION['userapellido'].'<span class="caret"></span></a> 
			 						

	                    		<ul class="dropdown-menu">
		                      		<li><a href="#"> <b>Rol:</b> '.$_SESSION['userrol'].'</a></li>
		                      		<li class="divider"></li>
			                      	<li><a href="#ClaveUsuario" data-toggle="modal">Cambiar Clave</a></li> 
			                        <li class="divider"></li>
			                        <li><a href="php/cerrar">Cerrar Sesión</a></li>
		                      	</ul>

	 							';            				 						
							}
	   				?>		         
		           
		            <!-- <li role="separator" class="divider"></li> -->
		            <!-- <li><a href="#" >Crear usuario</a></li>         -->
	          	</ul>
		        </li>
		      </ul>
		    </div><!-- /.navbar-collapse -->
	  	</div><!-- /.container-fluid -->
	</nav>