<!DOCTYPE html>
<html lang="en">
<head>
	<!-- Head -->
	<?php include 'html/head.php'; ?>

</head>
<body>
	
	<!-- Navbar -->
	<?php include 'html/navbar.php'; ?>
    
	<div class="container">
		<div id="info">	
			
				<div class="col-md-12 mx-auto text-center ">
					<img src="img/logo/logo.png" class="logo">
				</div>			

				<?php include("html/carrousel.php"); ?>
				
				<h2 class="font-weight-bold titulo">Los últimos dulces</h2>			
				<div class="row mb-5">  
					
					<?php 
						include("php/DulceFamily.php");
						$obj = new DulceFamily();
						$obj->UltimosProductos();
					?>
					 
				</div>				
       		
        </div>
	</div>


	<?php include 'html/footer.php'; ?>

	<!-- Modal Html para logearse -->
	<?php 
		include("html/modal_login.php");
		include("html/modal_edit.php");
	?>

	<script src="js/carrousel.js"></script>
	<script src="js/script.js"></script>
</body>
</html>                                		                            