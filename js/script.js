$(function() {
	load(1);
	loadUsuario(1);
});
function load(page){
	var query=$("#q").val();
	var per_page=10;
	var parametros = {"action":"ajax","page":page,'query':query,'per_page':per_page};
	$("#loader").fadeIn('slow');
	$.ajax({
		url:'ajax/listar_productos.php',
		data: parametros,
		 beforeSend: function(objeto){
		$("#loader").html("Cargando...");
	  },
		success:function(data){
			$(".outer_div").html(data).fadeIn('slow');
			$("#loader").html("");
		}
	})
}

$( "#add_product" ).submit(function( event ) {
  // var parametros = $(this).serialize();
  var form = new FormData($("#add_product")[0]);
	$.ajax({
			type: "POST",
			url: "ajax/guardar_producto.php",
			data: form,
			contentType:false,
			processData:false,
			 beforeSend: function(objeto){
				$("#resultados").html("Enviando...");
			  },
			success: function(datos){
			$("#resultados").html(datos);
			load(1);
			$('#addProductModal').modal('hide');
			$("#add_product")[0].reset();
		  }
	});
  event.preventDefault();
});

$('#editProductModal').on('show.bs.modal', function (event) {
  var button = $(event.relatedTarget) // Button that triggered the modal
  var nombre = button.data('nombre') 
  $('#edit_nombre').val(nombre)
  var descripcion = button.data('descripcion') 
  $('#edit_descripcion').val(descripcion)
  var costo = button.data('costo') 
  $('#edit_costo').val(costo)
  var imagen = button.data('imagen') 
  $('#edit_imagen').val(imagen)
  var id = button.data('id') 
  $('#edit_id').val(id)
})

$( "#edit_product" ).submit(function( event ) {
  // var parametros = $(this).serialize();
  var form = new FormData($("#edit_product")[0]);
	$.ajax({
			type: "POST",
			url: "ajax/editar_producto.php",
			data: form,
			contentType:false,
			processData:false,
			 beforeSend: function(objeto){
				$("#resultados").html("Enviando...");
			  },
			success: function(datos){
			$("#resultados").html(datos);
			load(1);
			$('#editProductModal').modal('hide');
		  }
	});
  event.preventDefault();
});

$('#deleteProductModal').on('show.bs.modal', function (event) {
  var button = $(event.relatedTarget) // Button that triggered the modal
  var id = button.data('id') 
  $('#delete_id').val(id)
})

$( "#delete_product" ).submit(function( event ) {
  var parametros = $(this).serialize();
	$.ajax({
			type: "POST",
			url: "ajax/eliminar_producto.php",
			data: parametros,
			beforeSend: function(objeto){
				$("#resultados").html("Enviando...");
			  },
			success: function(datos){
			$("#resultados").html(datos);
			load(1);
			$('#deleteProductModal').modal('hide');
		  }
	});
  event.preventDefault();
});

$( "#formularioSesion" ).submit(function( event ) {
  	// var parametros = $(this).serialize();
  	var form = new FormData($("#formularioSesion")[0]);
	$.ajax({
			type: "POST",
			url: "php/sesion.php",
			data: form,
			contentType:false,
			processData:false,
		success:function(msg){
		    $("#ver").show();
		    $("#ver").html(msg);
		}
	})

  event.preventDefault();

});

$( "#formularioContacto" ).submit(function( event ) {
  	//var parametros = $(this).serialize();
  	var form = new FormData($("#formularioContacto")[0]);
	$.ajax({
			type: "POST",
			url: "php/enviarCorreo.php",
			data: form,
			contentType:false,
			processData:false,
			beforeSend: function(objeto){
		    	$("#name").val('');
		    	$("#email").val('');
		    	$("#phone").val('');
		    	$("#message").val('');
		    },
			success:function(datos){
	    		$("#resultado").html(datos);
	    		$("#resultado").hide(10000);
	    	}
	})

  event.preventDefault();

});

function loadUsuario(page){
	var query=$("#usuario").val();
	var per_page=10;
	var parametros = {"action":"ajax","page":page,'query':query,'per_page':per_page};
	$("#loader").fadeIn('slow');
	$.ajax({
		url:'ajax/usuarios/listar_usuarios.php',
		data: parametros,
		 beforeSend: function(objeto){
		$("#loader_usuarios").html("Cargando...");
	  },
		success:function(data){
			$(".outer_div_usuarios").html(data).fadeIn('slow');
			$("#loader_usuarios").html("");
		}
	})
}

$( "#add_usuario" ).submit(function( event ) {
  // var parametros = $(this).serialize();
  var form = new FormData($("#add_usuario")[0]);
	$.ajax({
			type: "POST",
			url: "ajax/usuarios/guardar_usuario.php",
			data: form,
			contentType:false,
			processData:false,
			 beforeSend: function(objeto){
				$("#resultados").html("Enviando...");
			  },
			success: function(datos){
			$("#resultados").html(datos);
			loadUsuario(1);
			$('#addUsuarioModal').modal('hide');
		  }
	});
  event.preventDefault();
});

$('#editUsuarioModal').on('show.bs.modal', function (event) {
  var button = $(event.relatedTarget) // Button that triggered the modal
  var nombre = button.data('nombre') 
  $('#edit_nombreUsuario').val(nombre)
  var apellido = button.data('apellido') 
  $('#edit_apellido').val(apellido)
  var email = button.data('email') 
  $('#edit_email').val(email)
  var rol = button.data('rol') 
  $('#edit_rol').val(rol)
  var id = button.data('id') 
  $('#edit_idUsuario').val(id)
})

$( "#edit_usuario" ).submit(function( event ) {
  // var parametros = $(this).serialize();
  var form = new FormData($("#edit_usuario")[0]);
	$.ajax({
			type: "POST",
			url: "ajax/usuarios/editar_usuario.php",
			data: form,
			contentType:false,
			processData:false,
			 beforeSend: function(objeto){
				$("#resultados").html("Enviando...");
			  },
			success: function(datos){
			$("#resultados").html(datos);
			loadUsuario(1);
			$('#editUsuarioModal').modal('hide');
		  }
	});
  event.preventDefault();
});

$('#deleteUsuarioModal').on('show.bs.modal', function (event) {
  var button = $(event.relatedTarget) // Button that triggered the modal
  var id = button.data('id') 
  $('#delete_idUsuario').val(id)
  var email = button.data('email') 
  $('#delete_emailUsuario').val(email)
})

$( "#delete_usuario" ).submit(function( event ) {
  var parametros = $(this).serialize();
	$.ajax({
			type: "POST",
			url: "ajax/usuarios/eliminar_usuario.php",
			data: parametros,
			beforeSend: function(objeto){
				$("#resultados").html("Enviando...");
			  },
			success: function(datos){
			$("#resultados").html(datos);
			loadUsuario(1);
			$('#deleteUsuarioModal').modal('hide');
		  }
	});
  event.preventDefault();
});

$('#ClaveUsuario').on('show.bs.modal', function (event) {
  var button = $(event.relatedTarget) // Button that triggered the modal
  var clave = button.data('nclave') 
  $('#nclave').val(clave)
  var rclave = button.data('rclave') 
  $('#rclave').val(rclave)
  var clave_vieja = button.data('clave_vieja') 
  $('#clave_vieja').val(clave_vieja)
})

$( "#cambioClave" ).submit(function( event ) {
  // var parametros = $(this).serialize();
  var form = new FormData($("#cambioClave")[0]);
	$.ajax({
			type: "POST",
			url: "ajax/usuarios/editar_usuario.php",
			data: form,
			contentType:false,
			processData:false,
			beforeSend: function(objeto){
				$("#resultado").html("Enviando...");
			  },
			success: function(datos){
				$("#resultado").show();
				$("#resultado").html(datos);
				$("#cambioClave")[0].reset();

		  }
	});
  event.preventDefault();
});