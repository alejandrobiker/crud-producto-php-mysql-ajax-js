<?php 

// ini_set('display_errors', 0); // no muestra los errores


class DulceFamily {
		
	protected $id;
	protected $nombre;
	protected $descripcion;
	protected $costo;
	protected $imagen;
	protected $fecha;
	protected $dolar;

	function DulceFamily(){
		
			
	}

	function ObtenerProductoImagen(){

	}

	function RegistrarProducto() {

	}

	function ModificarProducto() {
		
	}

	function EliminarProducto() {
		
	}

	function ObtenerApiDolar(){
		ini_set('display_errors', 0);
		$json = file_get_contents('https://s3.amazonaws.com/dolartoday/data.json');
		$array = json_decode($json);
		$dolar = $array->USD->promedio_real;
		 // var_dump($dolar);
		 // die();
		return $dolar;
	}

	function ListarProducto(){
		
		include("php/conexion.php");
		$obj = new DulceFamily();
		$dolar = $obj->ObtenerApiDolar();
		
		$result = mysqli_query($con, "SELECT * FROM producto ORDER BY id DESC");
			
		while($extraido= mysqli_fetch_array($result)){

			$bolivares = $extraido['costo'] * $dolar;

			$bolivaresNew = number_format($bolivares, 0, ',', '.');

			echo '<div class="row mt-3">

					<div class="col-md-11 col-md-offset-1">						
				        <div class="col-lg-6 col-md-6 col-sm-12">            
				            <div class="">
				                <img src="ajax/'.$extraido['imagen'].'" class="img_product">
				                <p class="text-right">'.$extraido['fecha'].'</p>
				            </div>  
				        </div>
				        <div class="col-lg-6 col-md-6">				            
				            <p class="letra_titulo">'.$extraido['nombre'].'</p>';

				            if( $bolivaresNew > 0 && $extraido['costo'] > 0){

		            		echo '<p class="letra_costo">	
			            		BsS. '.$bolivaresNew.' / 
			            	  	$'. $extraido['costo'];
				            	 
							}else{
								echo '<p class="letra_costo">$'.$extraido['costo'];
							}

								echo '</p>
							<p><b>Descripción: </b>'.$extraido['descripcion'].'</p>	         			            						
			        	</div>
		    		</div>
		    	</div>
			';
		}
		mysqli_free_result($result);
		mysqli_close($con);
	}

	function UltimosProductos(){

		require_once("php/conexion.php");
		$result = mysqli_query($con, "SELECT * FROM producto ORDER BY id DESC LIMIT 4");

		while($extraido= mysqli_fetch_array($result)){
			echo '

			    <div class="col-lg-3 col-md-6 col-sm-12">
			        <div class="card-new">
					  	<div class="">
						    <img class="imagen_card" src="ajax/'.$extraido['imagen'].'" alt="Card image cap">
						    <a>
						      <div class="mask rgba-white-slight"></div>
						    </a>
					  	</div>

					 	<div class="card-body">
						    <p class="card-title text-center">'.$extraido['nombre'].' </p>
						    <p class="card-text text-center"> '. number_format($extraido['costo'],2) .' $</p>
						</div>
					</div> 
			    </div>       

		  	';
		}
		mysqli_free_result($result);
		mysqli_close($con);
	}

}

?>