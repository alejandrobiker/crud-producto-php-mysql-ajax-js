<?php
$nombre = $_POST['name'];
$mail = $_POST['email'];
$phone = $_POST['phone'];
$empresa = $_POST['message'];

$header = 'From: ' . $mail . " \r\n";
$header .= "X-Mailer: PHP/" . phpversion() . " \r\n";
$header .= "Mime-Version: 1.0 \r\n";
$header .= "Content-Type: text/html";

$from = 'jaramirez@soaint.com';
$asunto = 'Mensaje de mi sitio web';

$mensaje = '
        <html>
        <head>
        	<title></title>
        </head>
        <body>
            <h2>Haz recibido un mensaje a traves de Dulce Family</h2>
	        <p>Cliente: <b>' . $nombre . ' </b></p>
	        <p>E-mail: '. $mail .'</p>
	        <p>Número: '. $phone .'</p>
	        <h4>Mensaje: <br>'. $_POST['message'] .'</h4>
	        <br>
	        <p>Enviado: '. date('d/m/Y', time()) .' </p>
        </body>
        </html>
        ';

  	$envio = mail($from, $asunto, utf8_decode($mensaje), $header);
       
   // if product has been added successfully
    if ($envio) {
        $messages[] = "se ha enviado exitosamente, gracias por preferirnos!";
    } else {
        $errors[] = "Lo sentimos, no se ha enviado el correo, vuelva a intentarlo!";
    }

	if (isset($errors)){
			
	?>
	<div class="alert alert-danger" role="alert">
		<button type="button" class="close" data-dismiss="alert">&times;</button>
			<strong>Error!</strong> 
			<?php
				foreach ($errors as $error) {
						echo $error;
					}
			?>
	</div>
	<?php
	}
	if (isset($messages)){
		
	?>
		<div class="alert alert-success" role="alert">
				<button type="button" class="close" data-dismiss="alert">&times;</button>
				<strong>¡Bien hecho!</strong>
				<?php
					foreach ($messages as $message) {
							echo $message;
						}
					?>
		</div>
		<?php
	}

?>