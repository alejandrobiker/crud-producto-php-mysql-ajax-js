<?php include('php/seguridad.php');   ?>    

    <div class="table-wrapper">
        <div class="table-title">
            <div class="row">
                <div class="col-sm-6">
					<h2>Administrar Usuarios</h2>
				</div>
				<div class="col-sm-6">
					<a href="#addUsuarioModal" class="btn btn-rosado" data-toggle="modal"><i class="material-icons">&#xE147;</i> <span>Agregar nuevo usuario</span></a>
				</div>
            </div>
        </div>
		<div class='col-sm-4 pull-right'>
			<div id="custom-search-input">
                <div class="input-group col-md-12">
                    <input type="text" class="form-control" placeholder="Buscar"  id="usuario" onkeyup="loadUsuario(1);" />
                    <span class="input-group-btn">
                        <button class="btn btn-info" type="button" onclick="loadUsuario(1);">
                            <span class="glyphicon glyphicon-search"></span>
                        </button>
                    </span>
                </div>
            </div>
		</div>
		<div class='clearfix'></div>
		<hr>
		<div id="loader_usuarios"></div><!-- Carga de datos ajax aqui -->
		<div id="resultados"></div><!-- Carga de datos ajax aqui -->
		<div class='outer_div_usuarios'></div><!-- Carga de datos ajax aqui -->
        
		
    </div>
   
	<!-- Edit Modal HTML -->
	<?php include("html/modal_add.php");?>
	<!-- Edit Modal HTML -->
	<?php include("html/modal_edit.php");?>
	<!-- Delete Modal HTML -->
	<?php include("html/modal_delete.php");?>
	<script src="js/script.js"></script>
                             		                            